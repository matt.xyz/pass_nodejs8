//const { mostrarMenu, pausa } = require('./helpers/mensajes');
const { 
  inquirerMenu, 
  pausa, 
  leerInput,
  listadoTareasBorrar,
  confirmar,
  mostrarListadoChecklist
} = require('./helpers/inquirer');
const Tarea = require('./helpers/models/tarea');
const Tareas = require('./helpers/models/tareas');
require('colors');

const { guardarDB, leerDB } = require('./helpers/guardarArchivo')



  const main = async () => {
  console.log('hola mundo');

  let opt = '';
  const tareas = new Tareas();

  const tareasDB = leerDB() // el return data, viene aqui (array de Json)
console.log(tareasDB); //imprimiria esto, pero hay un clear en el Inquirer,JS

  if(tareasDB){
      //establecer las tareas
      tareas.cargarTareasFormArray(tareasDB);
  }

  // await pausa()


  do{
    opt = await inquirerMenu(); //de aqui imprime el titulo en verde, y se retorna la opcion seleccionada, en la variable "opt"

    console.log(opt); //saldra el numero de opcion seleccionada
    
    //const tarea = new Tarea('Comprar comida') //PRUEBAS
    // console.log(tareas); //PRUEBAS
    // if (opt !== '7') await pausa(); //PRUEBAS

    switch(opt) { //Switch de la opcion seleccionada.
      case '1': //entra aqui, cuando seleccionamos la primer opcion.
        //crear opcion

      console.log('MENU'.yellow);
      console.log('1.- Pizza Margarita'.green);
      console.log('2.- Pizza cuatro Estaciones'.green);
      console.log('3.- Pizza de Pepperoni'.green);
      console.log('4.- Pizza hawaiana'.green);
      console.log('5.- Pizza Napolitana'.green);

        const seleccion = await leerInput('Seleccione una Pizza:'); //en desc se guarda lo del LEERINPUT

      let desc;
      
      switch (seleccion) {
        case  '1': desc = 'Pizza Margarita'
        break;
        case '2': desc = 'Pizza cuatro Estaciones'
        break;       
        case '3': desc = 'Pizza de Pepperoni'
        break;   
        case '4': desc = 'Pizza hawaiana' 
        break;  
        case '5': desc = 'Pizza Napolitana'
        default: console.log('ingrese un dato correcto');
        break
      }

        console.log(desc); //imprime lo que escribimos
        tareas.crearTarea(desc)
      break;
      case '2':
        console.log(tareas.listadoArr);
         tareas.listadoCompleto(); //este pone una lista de numeros, sumandole al index +1
        //  console.log(tareas._listado);
      break;
      case '3':
        tareas.listarPendientesCompletadas(true)
      break
      case '4':
        tareas.listarPendientesCompletadas(false)
      break;
      case '5':
        const ids = await mostrarListadoChecklist(tareas.listadoArr);
        // console.log(ids);
        tareas.toggleCompletadas(ids);
      break;
      case '6':
        const id = await listadoTareasBorrar(tareas.listadoArr)

        if (id !== '0'){
          const ok = await confirmar('Estas seguro?');
        console.log({ok}); //con esto se ve si sale true or false
        // console.log('alba');
        if(ok){
          tareas.borrarTareas(id);
          console.log('Tarea borrada');
        }
        }
        break;

      default:
        break
    }

    guardarDB(tareas.listadoArr) //luego se guarda
    await pausa()
    
  } while(opt !== '7');

};

main();
