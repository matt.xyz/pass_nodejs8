const inquirer = require('inquirer')
require('colors');

const preguntas = [
  {
    type: 'list',
    name: 'opcion',
    message: 'que desea hacer?',
    choices: [
      {
        value: '1',
        name: `${'1.'.red} Realizar Pedido`
      },
      {
        value: '2',
        name: `${'2.'.red} Lista de pedidos`
      },
      {
        value: '3',
        name: `${'3.'.red} Pedidos Confirmados`
      },
      {
        value: '4',
        name: `${'4.'.red} Pedidos no Confirmados`
      },
      {
        value: '5',
        name: `${'5.'.red} Confirmar Pedidos`
      },
      {
        value: '6',
        name: `${'6.'.red} Eliminar Pedido`
      },
      {
        value: '7',
        name: `${'7.'.red} Concluir Pedido\n`
      },
    ]
  }
];

const inquirerMenu = async() => {

  console.clear();
  console.log('================================'.yellow);
  console.log('         Picasso Pizzería        '  .red);
  console.log('______________✩✩✩✩✩_____________'.yellow);
  console.log('          Realizar Pedido          '.red);
  console.log('================================'.yellow);
  const {opcion} = await inquirer.prompt(preguntas);
  return opcion;
};

const pausa = async() => {
  const question = [
    {
      type: 'input',
      name: 'enter',
      message: `Presione ${'enter'.green} para continuar`
    }
  ];
  console.log('\n');
  await inquirer.prompt(question);
}

const leerInput = async(message) => {

  const question = [
    {
      type: 'input', 
      name: 'desc',
      message,
      validate(value){  
        if(value.length === 0){
          return 'Por favor ingrese un valor';
        }
        return true;
      }
    }
  ];

 
  const { desc } = await inquirer.prompt(question);
  return desc;
};

  const listadoTareasBorrar = async(tareas = []) => {
    const choices = tareas.map((tarea, i) => {
      const idx = `${i + 1}`.green;

      return { 
        value: tarea.id,
        name: `${idx} ${tarea.desc}`,
      }
    });

  choices.unshift({
    value: '0',
    name: `0.'.green + ' Cancelar`
  })

  const preguntas = [
    {
      type: 'list',
      name: 'id',
      message: 'Borrar',
      choices
    }
  ];

  const { id } = await inquirer.prompt(preguntas);
  return id;
  }

  const confirmar = async (message) => {
    const question = [
      {
        type: 'confirm',
        name: 'ok',
        message
      }
    ];

    const { ok } = await inquirer.prompt(question);
    return ok;
  }

  const mostrarListadoChecklist = async(tareas = []) => {
    const choices = tareas.map((tarea, i) => {
      const idx = `${i + 1}.`.green;

      return {
        value: tarea.id,
        name: `${idx} ${tarea.desc}`,
        checked: ( tarea.completadoEn ) ? true : false
      }
    });
    const pregunta = [
      {
        type: 'checkbox',
        name: 'ids',
        message: 'Seleccione',
        choices
      }
    ];
    const { ids } = await inquirer.prompt(pregunta);
    return ids;
  }


module.exports= {
  inquirerMenu,
  pausa,
  leerInput,
  listadoTareasBorrar,
  confirmar,
  mostrarListadoChecklist
}