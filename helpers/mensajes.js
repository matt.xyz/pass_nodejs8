require('colors')

const mostrarMenu = async () => {

  return new Promise((resolve, reject) => {

    //limpiar consola
    console.clear();
    console.log('================================'.yellow);
    console.log('         Picasso Pizzería        '  .red);
    console.log('________________________________'.yellow);
    console.log('          Realizar Pedido          '.red);
    console.log('================================'.yellow);

  console.log(`1. Crear tareas`);
  console.log(`2. Listar tareas`);
  console.log(`3. Listar tareas completadas`);
  console.log(`4. Lstar tareas pendientes`);
  console.log(`5. Completar tarea(s)`);
  console.log(`6. Borrar tarea`);
  console.log(`7. Salir\n`);
  const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });

  readline.question('Seleccione una opcion: ', (opt) => {
    readline.close();
    resolve(opt);
  })

  });

};


  const pausa = () => {

    return new Promise((resolve, reject) => {
      const readline = require('readline').createInterface({
      input: process.stdin,
      output: process.stdout
    });

    readline.question(`\nPresione ${'ENTER'.green} para continuar\n`, () => {

      readline.close();
      resolve()
    })
    
    })
  }

module.exports = {
  mostrarMenu, //lo ponemos asi, y el por dentro hará esto --> mostrarMenu: mostrarMenu
  pausa
}